"use strict";
class WebForm
    {
        submit()
            {
                var date = new Date();
                                                            
                var reference = {
                    roomNumber: document.getElementById("roomNumber").value.toLowerCase(),
                    buildingAddress: document.getElementById("buildingAddress").value.toLowerCase(),
                    lightsOn: document.getElementById("lightsOn").value,
                    heatingCoolingOn: document.getElementById("heatingCoolingOn").value,
                    computersUsed: Number(document.getElementById("computersUsed").value),
                    computersTotal: Number(document.getElementById("computersTotal").value),
                    seatsUsed: Number(document.getElementById("seatsUsed").value),
                    seatsTotal: Number(document.getElementById("seatsTotal").value),
                    dateChecked: date.toISOString()
                }
                      
                var validate = {
                    roomNumValid: function()
                    {
                        if (reference.roomNumber === "")
                            {
                                document.getElementById("msg5").innerHTML = " *Enter a value!";
                            }
                        else 
                            {
                                document.getElementById("msg5").innerHTML = "";
                                validate.buildAddValid();
                            }
                    },
                    
                    buildAddValid: function()
                    {
                        if (reference.buildingAddress === "")
                            {
                                document.getElementById("msg6").innerHTML = " *Enter a value!";
                            }
                        else 
                            {
                                document.getElementById("msg6").innerHTML = "";
                                validate.compTotalValid();
                            }
                    },
                    
                    compTotalValid: function()
                    {
                        if (reference.computersTotal === "")
                            {
                                document.getElementById("msg1").innerHTML = " *Enter a value!";
                            }
                        else if (reference.computersTotal < 0)
                            {
                                document.getElementById("msg1").innerHTML = " *Value must be positive!";
                            }
                        
                        else
                            {
                                document.getElementById("msg1").innerHTML = "";
                                validate.compUsedValid();
                            }
                    },
                
                    compUsedValid: function()
                    {
                        if (reference.computersUsed === "")
                            {
                                document.getElementById("msg2").innerHTML = " *Enter a value!";
                            }
                        else if (reference.computersUsed < 0)
                            {
                                document.getElementById("msg2").innerHTML = " *Value must be positive!";
                            }
                        else if (reference.computersTotal - reference.computersUsed < 0 )
                            {
                                document.getElementById("msg2").innerHTML = " *Value must be less than Computers Total!";
                            }
                        else
                            {
                                document.getElementById("msg2").innerHTML = "";
                                validate.seatsTotalValid();
                            }
                    },
                
                    seatsTotalValid: function()
                    {
                        if (reference.seatsTotal === "")
                            {
                                document.getElementById("msg4").innerHTML = " *Enter a value!";
                            }
                        else if (reference.seatsTotal < 0)
                            {
                                document.getElementById("msg4").innerHTML = " *Value must be positive!";
                            }
                        else
                            {
                                document.getElementById("msg4").innerHTML = "";
                                validate.seatsUsedValid();
                            }
                    },
                
                    seatsUsedValid: function()
                    {   
                        if (reference.seatsUsed === "")
                            {
                                document.getElementById("msg3").innerHTML = " *Enter a value!";
                            }
                        else if (reference.seatsUsed < 0)
                            {
                                document.getElementById("msg3").innerHTML = " *Value must be positive!";
                            }
                        else if (reference.seatsTotal - reference.seatsUsed < 0 )
                            {
                                document.getElementById("msg3").innerHTML = " *Value must be less than Seats Total!";
                            }
                        else
                            {
                                document.getElementById("msg3").innerHTML = "";
                                var parameter = Object.values(reference);
                                var room1 = new RoomUsage(parameter);
                                console.log(room1.roomDetails);
                            }
                    }
                    
                }
                
            validate.roomNumValid();
        }
        
      